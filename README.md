# FullSim & FastSim dataset production

This repository is a setup to produce CMS MINIAODSIM datasets using either `FullSim` or `FastSim`.

## First installation

At DESY, you should load the CMSSW modules:
```
module use -a /afs/desy.de/group/cms/modulefiles/
module load cmssw
```

Then install `CMSSW_10_6_22`:
```
cmsrel CMSSW_10_6_22
```

Finally clone this repository in `$CMSSW_BASE/src`.

### In case of change of CMSSW version

The version of CMSSW may change in the future.
To track down the releases for the local architecture, just enter:
```
ls -1 -d /cvmfs/cms.cern.ch/*/cms/cmssw/CMSSW_10_6_22/src
```

## Execution

Start a new shell (to avoid possible conflicts), and load the environement:
```
source init
```

Since only the options of the `cmsDriver.py` are changing, the common scripts are present in the root directory and are accessible via soft links in the two subdirectories `Fast` and `Full`.
Choose any of the two directories depending on what you want to generate; the instructions are then identical.

### A first simple test

To run a small test, use the (local) `run` script.
```
./run 42
```
The option corresponds to the job id *and* to the seed of the random generator.
If it works without any problem (can take up to a few minutes), then you can proceed to the next step.

### Running parallel locally

One can run on all cores of the machines with `parallel`: this uses the cores of the machines without submitting any job.
Don't run more jobs than the number of cores (which you can get with `nproc`).
This cannot really be used for large scale production, but can be run for testing without submitting jobs on Condor.
Just do:
```
./parallel
```
No option is necessary.
You can change the number of events in the script itself.
Beware that each time you run this command, the former root files are removed.

This approach can be useful to ensure that different seed are used for each job (the second option of `run`, which we previously ignored).
Always check the occupancy of the local machine with `htop`, and don't go for this option if too many people are on this machine.

### Large scale submission on the cluster

Similar, just another script:
```
./submit
```
This should be the privileged approach for large-scale production.
Here too, remember that each time you rerun the command, you actually remove the former run.

In case you want to extend the statistics of some existing sample, just clone this repo and run it from scratch.

#### Troubleshooting

Check the status of your jobs:
```
condor_q
```

If your job is on hold and you want to know more:
```
condor_q -global -better-analyze JOBID
```
(You get the job id when running `condor_q`.)

If a or several jobs was or were put on hold, and if you could fix the issue, then you can release the job as follows:
```
condor_release -all
```

If you want to kill all your jobs:
```
condor_rm -all
```

Otherwise, consult the [official documentation](https://htcondor.readthedocs.io/en/latest/man-pages/index.html).
